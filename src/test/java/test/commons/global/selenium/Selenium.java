package test.commons.global.selenium;

import org.commons.selenium.TestDataProvider;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.testng.annotations.Test;



@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Selenium{

	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void selenium(String testdataName) {
		System.out.println(testdataName);
		TestProject project = new TestCases().getProject();
		TestProject project1 = new TestCases().getProject();
		project.loadUrl("https://www.google.com");
		project1.loadUrl("https://www.bing.com");
		project.closeAllBrowser();
		project1.closeAllBrowser();
		
	}

}