package test.commons.global;

import static org.junit.Assert.assertEquals;

import org.commons.selenium.property.DISProperties;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DIPropertiesTest {

	@Test
	public void getPropertyTest() {
		DISProperties diPropertiestest = DISProperties.getInstance();
		String propertyValue = diPropertiestest.getProperty("example1");
		assertEquals("Test", propertyValue);
		diPropertiestest.dispose();
	}

	@Test
	public void setandGetPropertyTest() {
		DISProperties diPropertiestest = DISProperties.getInstance();
		diPropertiestest.setProperty("test2", "$example2");
		String propertyValue = diPropertiestest.getProperty("test2");
		assertEquals(null, propertyValue);
		diPropertiestest.dispose();
	}

	@Test
	public void getFunctionalPropertyTest() {
		DISProperties diPropertiestest = DISProperties.getInstance();
		String functionalPropertyValue = diPropertiestest.getProperty("example");
		assertEquals("https://example.com", functionalPropertyValue);
		diPropertiestest.dispose();
	}

	@Test
	public void getTechnicalPropertyTest() {
		DISProperties diPropertiestest = DISProperties.getInstance();
		String technicalProperty = diPropertiestest.getTechnicalProperty("browser");
		assertEquals("firefox", technicalProperty);
		diPropertiestest.dispose();
	}

	@Test
	public void setTechnicalPropertyTest() {
		DISProperties diPropertiestest = DISProperties.getInstance();
		diPropertiestest.setTechnicalProperty("browser", "internetexplorer");
		String technicalProperty = diPropertiestest.getTechnicalProperty("browser");
		assertEquals("internetexplorer", technicalProperty);
		diPropertiestest.dispose();
	}

}