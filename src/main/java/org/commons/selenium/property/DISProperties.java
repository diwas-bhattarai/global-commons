package org.commons.selenium.property;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Properties;

import org.commons.constants.DIConstants;
import org.commons.properties.DIProperties;

public class DISProperties {
	private Properties property = null;
	private Properties propertyValue = null;
	private Properties technicalProperty = null;
	private Properties technicalPropertyValue = null;

	private DISProperties() {
		initiate();
	}

	private static class SingletonHelper {
		private static final DISProperties INSTANCE = new DISProperties();
	}

	public static DISProperties getInstance() {
		return SingletonHelper.INSTANCE;
	}

	public void initiate() {
		try {
			property = DIProperties
					.readProperty(Paths.get(DIConstants.PROPERTIES_PATH, DIConstants.PROPERTIES_FILE_PATH).toString());
			technicalProperty = DIProperties
					.readProperty(Paths.get(DIConstants.PROPERTIES_PATH, DIConstants.TECHNICAL_PROPERTIES).toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getProperty(String key) {
		String value = property.getProperty(key);
		if (value == null) {
			return null;
		}
		String valueSign = value.substring(0, 1);
		if (valueSign == "$" || valueSign.equals("$")) {
			value = getFunctionalValue(key, value);
		}
		return value;
	}

	public void setProperty(String key, String value) {
		propertyValue.setProperty(key, value);
	}

	public String getTechnicalProperty(String key) {
		String value = technicalProperty.getProperty(key);
		String valueSign = value.substring(0, 1);
		if (valueSign.equals("$")) {
			value = getTechnicalValue(key, value);
		}
		return value;
	}

	public void setTechnicalProperty(String key, String value) {
		technicalProperty.setProperty(key, value);
	}

	private String getFunctionalValue(String key, String value) {
		try {
			propertyValue = DIProperties
					.readProperty(Paths
							.get(DIConstants.PROPERTIES_PATH,
									DIConstants.PROPERTIES_FILE_PATH + DIConstants.PROPERTIES_VALUE_FILE_PATH)
							.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (propertyValue == null) {
			return value;
		} else {
			return propertyValue.getProperty(key + "." + value);
		}
	}

	private String getTechnicalValue(String key, String value) {
		try {
			technicalPropertyValue = DIProperties
					.readProperty(Paths
							.get(DIConstants.PROPERTIES_PATH,
									DIConstants.PROPERTIES_FILE_PATH + DIConstants.PROPERTIES_VALUE_FILE_PATH)
							.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (technicalPropertyValue == null) {
			return value;
		} else {
			return technicalPropertyValue.getProperty(key + "." + value);
		}
	}

	public void dispose() {
		if (property != null) {
			property = null;
		}
		if (propertyValue != null) {
			propertyValue = null;
		}
		if (technicalProperty != null) {
			technicalProperty = null;
		}
		if (technicalPropertyValue != null) {
			technicalPropertyValue = null;
		}
	}

}
