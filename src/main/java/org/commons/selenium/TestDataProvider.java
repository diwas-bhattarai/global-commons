package org.commons.selenium;

import java.lang.reflect.Method;

import org.commons.constants.DIConstants;
import org.testng.annotations.DataProvider;

public class TestDataProvider {

	@DataProvider(name = "dataProvider", parallel = true)
	public static Object[][] dataProviderMethod(Method m) {
		String testCaseName = m.getDeclaringClass().getSimpleName();
		String testdataFolder = DIConstants.TESTDATA_PATH;
		return new Object[][] { { "data one" } };
	}

}
