package org.commons.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.commons.constants.DIConstants;
import org.commons.logger.DILogger;
import org.commons.models.DIWebElements;
import org.commons.selenium.property.DISProperties;
import org.jboss.arquillian.graphene.DefaultGrapheneRuntime;
import org.jboss.arquillian.graphene.Graphene;
import org.jboss.arquillian.graphene.GrapheneRuntime;
import org.jboss.arquillian.graphene.context.GrapheneContext;
import org.jboss.arquillian.graphene.spi.configuration.GrapheneConfiguration;
import org.openqa.selenium.WebDriver;

public class DIWebPageActions extends DIWebPage {

	private static final Logger logger = LogManager.getLogger(DILogger.class);

	public DIWebPageActions(WebDriver driver) {
		super(driver);
	}

	public void load() {
		load(DISProperties.getInstance().getProperty(DIConstants.URL_PROPERTY));
	}

	public void load(String url) {
		logger.info("Loading the webpage: " + url);
		getDriver().get(url);
	}

	private void initiateGraphene(WebDriver driver) {
		try {
			GrapheneRuntime.popInstance();
			GrapheneContext.setContextFor(new GrapheneConfiguration(), driver, DIWebPageActions.class);
			GrapheneRuntime.pushInstance(new DefaultGrapheneRuntime());
		} catch (Exception e) {
			GrapheneContext.setContextFor(new GrapheneConfiguration(), driver, DIWebPageActions.class);
			GrapheneRuntime.pushInstance(new DefaultGrapheneRuntime());
		}
	}

	public void click(DIWebElements guiElement) {
		logger.info(guiElement);
		try {
			waitUntilElementAvailable(guiElement);

			if (guiElement.isAjax()) {
				try {
					initiateGraphene(getDriver());
					Graphene.guardAjax(getElement(guiElement)).click();
				} catch (Exception e) {
					logger.catching(e);
				}
			} else if (guiElement.isHttp()) {
				try {
					initiateGraphene(getDriver());
					Graphene.guardHttp(getElement(guiElement)).click();
				} catch (Exception e) {
					logger.catching(e);
				}
			} else {
				try {
					initiateGraphene(getDriver());
					Graphene.guardNoRequest(getElement(guiElement)).click();
				} catch (Exception e) {
					logger.catching(e);
				}
			}

		} catch (Exception e) {
			logger.error("Element not found: " + guiElement);
			System.err.println("Element not found: " + guiElement);
			e.printStackTrace();
		}
	}

	public void useValue(DIWebElements guiElement, String text) {
		waitUntilElementAvailable(guiElement);
		try {
			getElement(guiElement).sendKeys(text);
		} catch (Exception e) {
			logger.fatal("Element not Present: " + guiElement);
			e.printStackTrace();
		}
	}

	public void quit() {
		getDriver().quit();
	}

}
