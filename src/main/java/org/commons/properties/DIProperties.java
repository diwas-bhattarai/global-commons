package org.commons.properties;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DIProperties {

	public static Properties readProperty(String path) throws IOException, FileNotFoundException {
		Properties prop = new Properties();
		InputStream input = DIProperties.class.getClassLoader().getResourceAsStream(path);
		try {
			input = new FileInputStream(path);

			try {
				prop.load(input);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return prop;
	}

}
